exec { "yum-update":
  path    => '/usr/bin',
  command => "yum clean all; yum -q -y update --exclude cvs",
  timeout => 1800
}

#debian eske
# exec {'apt-update':
#   path    => '/usr/bin',
#   command => 'apt-get update'
# }

# class {'mysql::install': }
class {'nginx': }
class {'php5-fpm': }
class {'wordpress': }
# class {'phpmyadmin': }
